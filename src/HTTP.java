import java.util.List;

public class HTTP extends Protocol{
	
	private String lignesEnTete = "";
	
	public HTTP(List<String> requeste) { 
		super(requeste, 20);
		for (String string : requeste) {
			lignesEnTete += hexToString(string);
		}
	}

	private static String hexToString(String hexStr) { // conversion des data en ASCII
		StringBuilder str = new StringBuilder("");
		for(int i = 0; i<hexStr.length(); i+=2){
			String st = hexStr.substring(i, i+2);
			str.append((char) Integer.parseInt(st,16));
		}
		return str.toString();
	}
	
	
	public String toString() { 
		StringBuilder s = new StringBuilder();
		s.append("\nHTTP:");
		s.append("\n" +lignesEnTete);
		return s.toString();
	}

}
