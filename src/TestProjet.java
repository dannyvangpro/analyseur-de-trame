import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class TestProjet {

	private static List<String> readFile(String path) throws IOException{ //lecture du fichier
		List<String> liste_hex = new ArrayList<>();
		String offset = "";
		boolean firstOffset = false;
		boolean firstTimeRow = true;

		try {

			BufferedReader br = new BufferedReader(new FileReader(new File(path)));
			String line;

			while ((line = br.readLine()) != null) {			//lecture de la trame	

				if (!firstTimeRow && offset.equals("0000")) {	

					liste_hex.clear();							
				}

				for (String word : line.split(" ")) {			//lecture des octets

					if (firstOffset) {							
						offset = word;						
						firstOffset = false;				
					}

					if (word.equals("")) continue;				
					if (word.length() != 2) continue;			
					if (!word.matches("-?[0-9a-fA-F]+")) continue;

					liste_hex.add(word);						
				}

				firstOffset = true;							
				firstTimeRow = false;
			}
		}catch(IOException e) {
			e.printStackTrace();
		}
		return liste_hex;
	}
	

	private static void writeFile(String file,String outputf) throws IOException { //ecriture dans un fichier
		String s = lectureConsole(file);

		try {
			File fold = new File(outputf);
			if(!fold.exists()) {				
				fold.createNewFile();	
			}else {
				fold.delete();						
			}
			FileWriter fw = new FileWriter(outputf, true);
			BufferedWriter output = new BufferedWriter(fw);
			output.write(s);
			output.flush();						
			System.out.println("Write Succed"); 
			output.close();
		}catch (Exception e) {
			System.out.println("Erreur d'ecriture du fichier" + e );
		}

	}
	

	private static String lectureConsole(String file) throws IOException { //affichage sur la console
		String s ="";
		Ethernet e = new Ethernet(TestProjet.readFile(file));
		s+= e.toString();

		IP ip = new IP(e.getData());
		s+= ip.toString();

		TCP tcp = new TCP(ip.getData());
		s+= tcp.toString();

		HTTP http = new HTTP(tcp.getData());
		s+= http.toString();

		s+= "\n";
		return s;
	}
	

	public static void main(String[] args) throws IOException {

		System.out.print("Bienvenue:\nVeuillez entrer dans le nom du fichier:");
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
		String file = reader.readLine();


		System.out.println(lectureConsole(file));
		writeFile(file,"output.txt");

		System.out.println("Fin\nA Bient�t");

	}

} 
