import java.util.List;

public abstract class Protocol {
    protected final int headerLength;
    private final List<String> payload; // la trame 
    protected final List<String> data;	// l'ensemble des donn�es

    public Protocol(List<String> data, int headerLength) {
        this.data = data;
        this.headerLength = headerLength;
        this.payload = data.subList(headerLength,getSize());
    }
    
    public int getSize() {
    	return data.size();
    }

    public List<String> getPayload() {
        return payload;
    }
}
