import java.util.List;

public class TCP extends Protocol{
	
	private String sourcePortNumber;
	private String destPortNumber;
	private String sequenceNumber;
	private String acknowledgmentNumber;
	private String THL;
	private String flag;
	private char URG;
	private char ACK;
	private char PSH;
	private char RST;
	private char SYN;
	private char FIN;
	private String window;
	private String checksum;
	private String urgentPointer;
	
	
	public TCP(List<String> data) { 
		super(data,32);
		
		sourcePortNumber = data.get(0) + data.get(1);
		destPortNumber = data.get(2) + data.get(3);
		sequenceNumber= data.get(4) + data.get(5)+ data.get(6)+data.get(7);
		acknowledgmentNumber = data.get(8) + data.get(9) + data.get(10) + data.get(11);
		THL = data.get(12);
		flag = data.get(12)+data.get(13);
		window = data.get(14) + data.get(15);
		checksum = data.get(16) + data.get(17);
		urgentPointer= data.get(18) +data.get(19);
		initFlag();
	}
	
	public void initFlag() {		//initialisation des flags 
		char hex = data.get(13).charAt(1);
		char hex1 = data.get(13).charAt(0);
		
		Integer hexI = Integer.parseInt(Character.toString(hex));
		String hexS = String.format("%2s", Integer.toBinaryString(hexI)).replaceAll(" ","0");
		
		URG= hexS.toString().charAt(1);
		ACK =hexS.toString().charAt(0);
		
		hexI = Integer.parseInt(Character.toString(hex1));
		hexS = String.format("%4s", Integer.toBinaryString(hexI)).replaceAll(" ","0");
		
		PSH = hexS.toString().charAt(3);
		RST = hexS.toString().charAt(2);
		SYN = hexS.toString().charAt(1);
		FIN = hexS.toString().charAt(0);
	}
	
	public List<String> getData() {
		return data.subList(20 ,data.size());
	} 
	
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append("\nTCP:");
		s.append("\n\tSource Port: "+ Integer.parseInt(sourcePortNumber,16));
		s.append("\n\tDestination Port: "+  Integer.parseInt(destPortNumber,16));
		s.append("\n\tSequence Number: "+ sequenceNumber);
		s.append("\n\tAcknowledgment Number: "+ acknowledgmentNumber);
		s.append("\n\tHeader Length: "+ THL);
		s.append("\n\tFlags : "+ flag);
		s.append("\n\t  URG: "+ URG);
		s.append("\n\t  ACK: "+ ACK);
		s.append("\n\t  PSH: "+ PSH);
		s.append("\n\t  RST: "+ RST);
		s.append("\n\t  SYN: "+ SYN);
		s.append("\n\t  FIN: "+ FIN);
		s.append("\n\tWindow : " + window);
		s.append("\n\tchecksum : 0x" + checksum);
		s.append("\n\tUrgent Pointer : "+Integer.parseInt( urgentPointer,16));

		return s.toString();
	}

}
