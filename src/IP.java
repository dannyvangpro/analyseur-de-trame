import java.util.ArrayList;
import java.util.List;

public class IP extends Protocol {
	private static final int version=4;
	private String ipHeaderLength;
	private String typeOfService;
	private String totalLength;
	private String id;
	private String flags;
	private char flagR;
	private char flagDF;
	private char flagMF;
	private char fragmentOffset;
	private String timeToLive;
	private String protocol;
	private String headChecksum;
	private String sourceIP;
	private String destinationIP;

	
	private boolean checksum;
	

	public IP(List<String> data) {
		super(data,20); 
		
		ipHeaderLength=Character.toString(data.get(0).charAt(1));
		typeOfService = data.get(1);
		totalLength=data.get(2) + data.get(3);
		id=data.get(4)+data.get(5);
		flags = data.get(6)+ data.get(7);
		timeToLive=data.get(8);
		protocol=data.get(9);
		headChecksum=data.get(10) + data.get(11);
		
		sourceIP= sourip();
		destinationIP=destip();
		
		initFlagsAndFragmentOffset();
		checksum = checkSum();
		
	}
	
	public void initFlagsAndFragmentOffset() {		//initialisation des flags et offset
		char hex = data.get(6).charAt(0);
		Integer hexI = Integer.parseInt(Character.toString(hex));
		String hexS = String.format("%4s", Integer.toBinaryString(hexI)).replaceAll(" ","0");
		
		flagR=hexS.toString().charAt(0);
		flagDF=hexS.toString().charAt(1);
		flagMF=hexS.toString().charAt(2);
		fragmentOffset = hexS.toString().charAt(3);

	}
	public String sourip() {  		//Adresse MAC source 
		String t="" ;
		
		for(int i=12;i<16;i++) {
			if(i!=15){			
				t=t.concat(Integer.parseInt(data.get(i),16)+":");
				}
			else{			
				t=t.concat(Integer.parseInt(data.get(i),16)+"");

				}
		}
		
		
		return t;
	}
	
	
	public String destip() { 	//Adresse MAC dest
		String t="" ;
		for(int i=16;i<20;i++) {
			if(i!=19){			
				t=t.concat(Integer.parseInt(data.get(i),20)+":");
				}
			else{			
				t=t.concat(Integer.parseInt(data.get(i),20)+"");

				}
		}
		
		
		return t;
	}
	private boolean checkSum() {		//verification du checksum
		
		List<String> header = new ArrayList<>();
		header.addAll(data.subList(0,20));
		List<String> hexa = new ArrayList<>();
		
		for(int i = 0; i<header.size(); i+=2){
			hexa.add(header.get(i) + header.get(i+1));
		}
		
		int sum=0;
		for(String hex: hexa) {
			sum+=Integer.parseInt(hex,16);
		}
		
		if(sum>0xFFFF) {
			sum = (sum >> 16) + (sum & 0xFFFF);
		}
		
		return sum == 0xFFFF;
		
	}
	
	public String addChar(String str, String string, int position) {
	    StringBuilder sb = new StringBuilder(str);
	    for(int i=position;i<sb.length();i+=position+1) {
	    	sb.insert(i, string);
	    }
	    
	    return sb.toString();
	}
	
	public List<String> getData() {		//retour du data pour la trame suivante
		return data.subList(Integer.parseInt(ipHeaderLength)*4 ,data.size());
	}
	
	
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append("IPv4:");
		s.append("\n\tVersion: "+ version);
		s.append("\n\tIHL: " + 4*Integer.parseInt(ipHeaderLength) + " bytes (0x" +ipHeaderLength+ ")");
		s.append("\n\ttypeOfService: Ox" +typeOfService);
		s.append("\n\ttotalLength: " + totalLength);
		s.append("\n\tIdentical: "+Integer.parseInt(id,16) + " (0x" + id+ ")");
		s.append("\n\tTTL: " +Integer.parseInt(timeToLive,16));
		s.append("\n\tProtocol:0x" +protocol);
		s.append("\n\tHeader checksum:" + headChecksum);
		s.append("\n\tFlag: 0x" + flags);
		s.append("\n\t  FlagR:" +flagR);
		s.append("\n\t  FlagDF:" +flagDF);
		s.append("\n\t  FlagMF:" +flagMF);
		s.append("\n\tFlagmentOffset: "+fragmentOffset);
		s.append("\n\tchecksum:" +checksum);
		s.append("\n\tIP Source: " + sourceIP);
		s.append("\n\tIP Dest : " +  destinationIP);

		return s.toString();
	}
		
}
	
