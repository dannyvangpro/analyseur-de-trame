import java.util.List;


public class Ethernet extends Protocol {
	private final String destMac;
	private final String souMac;
	private final String type;
	
	
	public Ethernet(List<String> data) {
		super(data,14);
		souMac = soumac();; //hexa
		destMac = desmac(); //hexa
		type= typage();
	}
	
	public String addChar(String str, String string, int position) {
	    StringBuilder sb = new StringBuilder(str);
	    for(int i=position;i<sb.length();i+=position+1) {
	    	sb.insert(i, string);
	    }
	    
	    return sb.toString();
	}
	
	public String typage() { //Type
		String t=data.get(12)+data.get(13) ;
			
		if (t.equals("0800")){
			 t = "0800 : ipv4";
		}else if(t.equals("0806")){
			 t = "0806 : arp";
		}else {
			t = " erreur";
		}
		return t;
	}
	
	
	public String soumac() { // Adresse ip Source
		String t="" ;
		
		for(int i=0;i<6;i++) {
			t=t.concat(data.get(i));
			}

		return t;
	}
	
	
	public String desmac() { //Adresse ip Destination
		String t="" ;
		for(int i=6;i<12;i++) {
			t=t.concat(data.get(i));
			}
		
		return t;
	}
	

	public List<String> getData() {
		return data.subList(14,data.size());
	}
	
	
	public String toString() { 
		StringBuilder s = new StringBuilder();
		s.append("Trame Ethernet: ");
		s.append("\n\tSourceMac: "+addChar(souMac, ":", 2));
		s.append("\n\tDestinationMac : "+addChar(destMac, ":", 2));
		s.append("\n\tType:Ox"+type);
		s.append("\n");
		return s.toString();
	}

	

}
